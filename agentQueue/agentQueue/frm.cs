﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimpleHttp;

namespace agentQueue
{
    public partial class frm : Form
    {
        public string KIOSK_CODE = System.Configuration.ConfigurationManager.AppSettings["KIOSK_CODE"];
        public string IP_SERVER  = System.Configuration.ConfigurationManager.AppSettings["IP_SERVER"];
        public string PORT       = System.Configuration.ConfigurationManager.AppSettings["PORT"];
        public frm()
        {
            InitializeComponent();
        }


        private void frm_Load(object sender, EventArgs e)
        {
            timerShutdown.Interval = 1500; //1000 = 1 วินาที
            //timerShutdown.Start();
            //while (true) 
            //{
            //    //Console.WriteLine(PerformanceInfo.GetPerformanceInfo());
            //}
            //Console.WriteLine(PerformanceInfo.checkPrinterStatus());
            

            backgroundWorker1.RunWorkerAsync();
        }


        private void setNetSH(string port)
        {
            string str = "netsh http add urlacl url=http://+:"+ PORT + "/ user=\"Everyone\"";

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C " + str;
            startInfo.Verb = "runas";
            process.StartInfo = startInfo;
            process.Start();
        }

        private string RunScript(String runScript) 
        {
            Runspace runspace = RunspaceFactory.CreateRunspace();
            runspace.Open();
            Pipeline pipeline = runspace.CreatePipeline();
            pipeline.Commands.AddScript(runScript);
            pipeline.Commands.Add("Out-String");
            Collection<PSObject> results = pipeline.Invoke();
            runspace.Close();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (PSObject pSObject in results)
                stringBuilder.AppendLine(pSObject.ToString());
            return stringBuilder.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            String iDate = DateTime.Now.ToString("yyyy/MM/dd") + " 23:35";

            Console.WriteLine(PerformanceInfo.GetPerformanceInfo());

            DateTime a = DateTime.Now;
            DateTime b = Convert.ToDateTime(iDate);

            if (b.Subtract(a).TotalMinutes < 0)
            {
                //Console.WriteLine(b.Subtract(a).TotalMinutes);
                Utils.Shutdown();
            }
            else 
            {
                //Console.WriteLine(b.Subtract(a).TotalMinutes);
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            startService();
        }

        private  void startService() 
        {
            try
            {
                Route.Add("/", (req, res, props) =>
                {
                    res.AsText("Welcome to the Queue Http Server");
                });

                Route.Add("/ws/", (req, res, props) =>
                {
                    res.AsText("Welcome to the Queue Http Server");
                });

                Route.Add("/ws/printQueue/{path}", (req, res, props) =>
                {
                    string path = props["path"];
                    //MessageBox.Show(path);

                    Process p = new Process();
                    p.StartInfo = new ProcessStartInfo()
                    {
                        CreateNoWindow = true,
                        Verb = "print",
                        FileName = path //put the correct path here
                    };
                    p.Start();

                    res.AsText("");
                });

                Route.Add("/ws/GetPerformanceInfo/{kioskCode}", (req, res, props) =>
                {
                    string kioskCode = props["kioskCode"];
                    //var data = new StringContent(PerformanceInfo.GetPerformanceInfo(), Encoding.UTF8, "application/json"); 
                    //res.AsText(PerformanceInfo.GetPerformanceInfo(), "application/json");

                    byte[] data = Encoding.UTF8.GetBytes(PerformanceInfo.GetPerformanceInfo());

                    res.ContentType = "application/jsonl";
                    res.ContentEncoding = Encoding.UTF8;
                    res.ContentLength64 = data.LongLength;

                    // Write out to the response stream (asynchronously), then close it
                    res.OutputStream.WriteAsync(data, 0, data.Length);
                });



                HttpServer.ListenAsync(
                int.Parse(PORT),
                CancellationToken.None,
                Route.OnHttpRequestAsync

                ).Wait();
            }
            catch (Exception ex)
            {
                setNetSH(PORT);
                Thread.Sleep(2000);
                startService();
                Console.WriteLine(ex.ToString());
            }
        }
    }
}